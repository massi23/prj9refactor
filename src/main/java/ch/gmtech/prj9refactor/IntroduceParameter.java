package ch.gmtech.prj9refactor;

import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

public class IntroduceParameter {
	
	class Entry {
		  
	    Entry (double value, Date chargeDate) {
	        _value = value;
	        _chargeDate = chargeDate;
	    }
	    
	    Date getDate(){
	        return _chargeDate;
	    }
	    
	    double getValue(){
	        return _value;
	    }
	    
	    private final Date _chargeDate;
	    private final double _value;
	  }
	  
	  class Account{
	    double getFlowBetween (DateRange aDateRange) {
	        double result = 0;
	        Enumeration e = _entries.elements();
	        while (e.hasMoreElements()) {
	            Entry each = (Entry) e.nextElement();
	            if (aDateRange.includes(each.getDate()))
	            {
	            }
	        }
	        return result;
	    }
	    
	    private final Vector _entries = new Vector();
	  }
	  
	  class Client {
		  void doStuff() {
			  new Account().getFlowBetween(new DateRange(new Date(), new Date()));
		  }
	  }
	  
	  class DateRange {

		private final Date _endDate;
		private final Date _startDate;

		public DateRange(Date startDate, Date endDate) {
			_startDate = startDate;
			_endDate = endDate;
		}

		public boolean includes(Date aDate) {
			if(aDate.equals(getStart())) return true;
			if(aDate.equals(getEnd())) return true;
			if(aDate.after(getStart()) && aDate.before(getEnd())) return true;
			return false;
		}

		public Date getEnd() {
			return _endDate;
		}

		public Date getStart() {
			return _startDate;
		}
		  
	  }

}
