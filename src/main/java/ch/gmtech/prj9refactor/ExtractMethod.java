package ch.gmtech.prj9refactor;

public class ExtractMethod {

	class Order {
		private String _name;

		void printOwing(double amount) {
			printBanner();
			printDetails(amount);
		}

		private void printDetails(double amount) {
			System.out.println ("name:" + _name);
			System.out.println ("amount" + amount);
		}

		private void printBanner() {
			System.out.println("banner");
		}
	}
	
	
}
