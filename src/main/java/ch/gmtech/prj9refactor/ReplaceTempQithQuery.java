package ch.gmtech.prj9refactor;

public class ReplaceTempQithQuery {
	
	class Items {
		private final int _quantity;
		private final int _itemPrice;
		
		public Items(int quantity, int itemPrice) {
			_quantity = quantity;
			_itemPrice = itemPrice;
		}

		double getPrice() {
	        return basePrice() * discountFactor();
	    }

		private double discountFactor() {
			return (basePrice() > 1000) ? 0.95 : 0.98;
		}

		private int basePrice() {
			return _quantity * _itemPrice;
		}
	}

	
	
}
