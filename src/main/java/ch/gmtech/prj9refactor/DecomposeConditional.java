package ch.gmtech.prj9refactor;

import java.util.Date;

public class DecomposeConditional {
	class Decompose {
		Date SUMMER_START = new Date();
		Date SUMMER_END = new Date();
		private int _winterRate;
		private int _summerRate;
		private int _winterServiceCharge;

		public int doSomething() {
			Date date = new Date();
			int quantity = 10;
			return isWinter(date) ? computeWinterCharge(quantity) : computerSummerCharge(quantity);
		}

		private int computerSummerCharge(int quantity) {
			return quantity * _summerRate;
		}

		private int computeWinterCharge(int quantity) {
			return quantity * _winterRate + _winterServiceCharge;
		}

		private boolean isWinter(Date date) {
			return date.before (SUMMER_START) || date.after(SUMMER_END);
		}
	}

}
