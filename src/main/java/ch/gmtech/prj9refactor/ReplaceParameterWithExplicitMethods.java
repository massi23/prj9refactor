package ch.gmtech.prj9refactor;


public class ReplaceParameterWithExplicitMethods {
	/*
	class Employee {
		static final int ENGINEER = 0;
		static final int SALESMAN = 1;
		static final int MANAGER = 2;
		
		Employee create(int type) {
			switch (type) {
				case ENGINEER: 
					return new Engineer();
				case SALESMAN:
					return new Salesman();
				case MANAGER:
					return new Manager();
				default:
					throw new IllegalArgumentException("Incorrect type code value");
			}
		}
	}
	public class Engineer extends Employee{}
	public class Salesman extends Employee{}
	public class Manager extends Employee{}
	*/

	class Employee {
		static final int ENGINEER = 0;
		static final int SALESMAN = 1;
		static final int MANAGER = 2;
		
		public Manager createManager() {return new Manager();}
		public Salesman createSalesman() {return new Salesman();}
		public Employee createEngineer() { return new Engineer();}
	}
	public class Engineer extends Employee{}
	public class Salesman extends Employee{}
	public class Manager extends Employee{}
}
