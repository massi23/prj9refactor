package ch.gmtech.prj9refactor;

public class ReplaceMethodWithMethodObject {
	
	class Account{
	    int gamma (int inputVal, int quantity, int yearToDate) {
	    	return new GammaCalculator(this, inputVal, quantity, yearToDate).compute();
	    }
		private int delta() {
			return 0;
		}
	 }
	
	class GammaCalculator {

		private final Account _account;
		private final int _inputVal;
		private final int _quantity;
		private final int _yearToDate;

		public GammaCalculator(Account anAccount, int anInputVal, int aQuantity, int aYearToDate) {
			_account = anAccount;
			_inputVal = anInputVal;
			_quantity = aQuantity;
			_yearToDate = aYearToDate;
		}

		public int compute() {
			int importantValue1 = (_inputVal * _quantity) + _account.delta();
			int importantValue2 = (_inputVal * _yearToDate) + 100;
			if((_yearToDate - importantValue1) > 100 ) importantValue2 -= 20;
			
			int importantValue3 = importantValue2 * 7;
			return importantValue3 - 2 * importantValue1;
		}
		
	}

}
