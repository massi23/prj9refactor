package ch.gmtech.prj9refactor;

public class ExtractSubclass {
	
	class JobItem {
			private final int _unitPrice;
			private final int _quantity;
			private final Employee _employee;
			
			public JobItem(int unitPrice, int quantity, Employee employee) {
				_unitPrice = unitPrice;
				_quantity = quantity;
				_employee = employee;
			}
			public int getTotalPrice() {
				return getUnitPrice() * _quantity;
			}
			public int getUnitPrice() {
				return _unitPrice;
			}
			public int getQuantity() {
				return _quantity;
			}
			public Employee getEmployee() {
				return _employee;
			}
		}
	
		class LaborItem extends JobItem {

			public LaborItem(int aQuantity, Employee aEmployee) {
				super(-1, aQuantity, aEmployee);
			}
			
			@Override
			public int getUnitPrice() {
				return getEmployee().getRate();
			}
		}
	
		class Employee {
			private final int _rate;
			
			public Employee(int rate) {
				_rate = rate;
			}
			public int getRate() {
				return _rate;
			}
		}

}
