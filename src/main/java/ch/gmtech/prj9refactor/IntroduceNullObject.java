package ch.gmtech.prj9refactor;

public class IntroduceNullObject {
	/*
	class Site {
		Customer _customer;

		void setCustomer(Customer aCustomer) {
			_customer = aCustomer;
		}
		Customer getCustomer() {
			return _customer;
		}
	}
	
	class Customer {
		public String getName() {return "mario";}
		public BillingPlan getPlan() { return new BillingPlan();}
		public PaymentHistory getHistory() { return new PaymentHistory();}
	}
	public static class BillingPlan {

		public static BillingPlan basic() {
			return null;
		}
	}
	class PaymentHistory {
		int getWeeksDelinquentInLastYear() { return  1000;}
	}
	
	class MyBlaBla {
		public void doSomething() {
			Site site = new Site();
			Customer customer = site.getCustomer();
			
			BillingPlan plan;
			if (customer == null) {
				plan = BillingPlan.basic();
			} else {
				plan = customer.getPlan();
			}
	
			String customerName;
			if (customer == null)  {
				customerName = "occupant";
			} else {
				customerName = customer.getName();
			}
			
			int weeksDelinquent;
			if (customer == null) {
				weeksDelinquent = 0;
			} else {
				weeksDelinquent = customer.getHistory().getWeeksDelinquentInLastYear();
			}
		}
		
	}
	*/
	class Site {
		Customer _customer;
		public Site() {
			setCustomer(new NullCustomer());
		}

		void setCustomer(Customer aCustomer) {
			_customer = aCustomer;
		}
		Customer getCustomer() {
			return _customer;
		}
	}
	
	class Customer {
		public String getName() {return "mario";}
		public BillingPlan getPlan() { return new BillingPlan();}
		public PaymentHistory getHistory() { return new PaymentHistory();}
	}
	class NullCustomer extends Customer {
		@Override
		public String getName() {return "occupant";}
		@Override
		public BillingPlan getPlan() { return BillingPlan.basic();}
		@Override
		public PaymentHistory getHistory() { return new PaymentHistory();}
	}
	public static class BillingPlan {

		public static BillingPlan basic() {
			return null;
		}
	}
	class PaymentHistory {
		int getWeeksDelinquentInLastYear() { return  1000;}
	}
	
	class MyBlaBla {
		public void doSomething() {
			Site site = new Site();
			Customer customer = site.getCustomer();
			BillingPlan plan = customer.getPlan();
			String customerName = customer.getName();
			int weeksDelinquent = customer.getHistory().getWeeksDelinquentInLastYear();
		}
	}
}
