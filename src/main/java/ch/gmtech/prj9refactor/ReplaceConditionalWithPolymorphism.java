package ch.gmtech.prj9refactor;


public class ReplaceConditionalWithPolymorphism {
	
	/*
	class Employee {
		EmployeeType _type;
		private int _monthlySalary;
		private int _commission;
		private int _bonus;
		
		int payAmount() {
			switch (getType()) {
				case EmployeeType.ENGINEER:
					return _monthlySalary;
				case EmployeeType.SALESMAN:
					return _monthlySalary + _commission;
				case EmployeeType.MANAGER:
					return _monthlySalary + _bonus;
				default:
					throw new RuntimeException("Incorrect Employee");
			}
		}
		
		int getType() {
			return _type.getTypeCode();
		}
	}

	abstract  class EmployeeType {
		public static final int ENGINEER = 0;
		public static final int SALESMAN = 1;
		public static final int MANAGER = 2;
		public abstract int getTypeCode();
	}
	
	class Engineer extends EmployeeType {
		@Override
		public int getTypeCode() {
			return EmployeeType.ENGINEER;
		}
	}
	
	class Salesman extends EmployeeType {
		@Override
		public int getTypeCode() {
			return EmployeeType.SALESMAN;
		}
	}
	
	class Manager extends EmployeeType {
		@Override
		public int getTypeCode() {
			return EmployeeType.MANAGER;
		}
	}
	*/
	
	class Employee {
		EmployeeType _type;
		
		public Employee(EmployeeType type) {
			_type = type;
		}
		
		int payAmount() {
			return _type.payAmount();
		}

	}

	abstract  class EmployeeType {
		public abstract int payAmount();
	}
	
	class Engineer extends EmployeeType {
		private final int _monthlySalary;
		
		public Engineer(int monthlySalary) {
			_monthlySalary = monthlySalary;
		}
		
		@Override
		public int payAmount() {
			return _monthlySalary;
		}
	}
	
	class Salesman extends EmployeeType {
		private final int _monthlySalary;
		private final int _commission;

		public Salesman(int monthlySalary, int commission) {
			_monthlySalary = monthlySalary;
			_commission = commission;
		}
		@Override
		public int payAmount() {
			return _monthlySalary + _commission;
		}
	}
	
	class Manager extends EmployeeType {
		private final int _monthlySalary;
		private final int _bonus;
		
		public Manager(int monthlySalary, int bonus) {
			_monthlySalary = monthlySalary;
			_bonus = bonus;
		}
		@Override
		public int payAmount() {
			return _monthlySalary + _bonus;
		}
	}

}
