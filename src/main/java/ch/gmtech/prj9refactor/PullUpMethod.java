package ch.gmtech.prj9refactor;

import java.util.Date;

public class PullUpMethod {
	
	abstract class Customer {
		protected Date lastBillDate;
		void createBill(Date date) {
			double chargeAmount = chargeFor(lastBillDate, date);
			addBill(date, chargeAmount);
		}
		protected abstract double chargeFor(Date aLastBillDate, Date aDate);
		public void addBill(Date date, double amount) {
		}
	}
	
	class RegularCustomer extends Customer {
		@Override
		protected double chargeFor(Date lastBillDate, Date date) {
			return 1;
		}
	}
	
	class PreferredCustomer extends Customer {
		@Override
		protected double chargeFor(Date lastBillDate, Date date) {
			return 10;
		}
	}

	
	
}
