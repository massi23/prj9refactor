package ch.gmtech.prj9refactor;

public class MoveMethod {
/*
	class Account {
		private AccountType _type;
		private int _daysOverdrawn;

		double overdraftCharge() {
			if (_type.isPremium()) {
				double result = 10;
				if (_daysOverdrawn > 7)
					result += (_daysOverdrawn - 7) * 0.85;
				return result;
			} else
				return _daysOverdrawn * 1.75;
		}

		double bankCharge() {
			double result = 4.5;
			if (_daysOverdrawn > 0)
				result += overdraftCharge();
			return result;
		}
	}
	
	class AccountType {
		public boolean isPremium() {
			return false;
		}
	}
	
	*/
	
	class Account {
		private AccountType _type;
		private int _daysOverdrawn;

		double overdraftCharge() {
			return _type.overdraftCharge(_daysOverdrawn);
		}

		double bankCharge() {
			double result = 4.5;
			if (_daysOverdrawn > 0)
				result += overdraftCharge();
			return result;
		}
	}
	
	class AccountType {
		double overdraftCharge(int daysOverdrawn) {
			if (isPremium()) {
				double result = 10;
				if (daysOverdrawn > 7)
					result += (daysOverdrawn - 7) * 0.85;
				return result;
			} else
				return daysOverdrawn * 1.75;
		}
		public boolean isPremium() {
			return false;
		}
	}

}
