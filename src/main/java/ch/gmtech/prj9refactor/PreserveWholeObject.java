package ch.gmtech.prj9refactor;

public class PreserveWholeObject {
	
	public class TempRange {
		public int getLow() {
			return 0;
		}
		
		public int getHigh() {
			return 0;
		}
	}
	class Room {
	    boolean withinPlan(HeatingPlan plan) {
	        return plan.withinRange(daysTempRange());
	    }
		private TempRange daysTempRange() {
			return new TempRange();
		}
	}
	    
	class HeatingPlan{
		private TempRange _range;
		
	    boolean withinRange (TempRange aTempRange) {
	        return (aTempRange.getLow() >= _range.getLow() && aTempRange.getHigh() <= _range.getHigh());
	    }
	}

}
