package ch.gmtech.prj9refactor;

public class ExtractClass {

	class Person {
		private String _name;
		private TelephoneNumber _telephoneNumber;
		
		
		public String getName() {
			return _name;
		}
		
		public String getTelephoneNumber() {
			return _telephoneNumber.getNumber();
		}
		
		String getOfficeAreaCode() {
			return _telephoneNumber.getOfficeAreaCode();
		}
		void setOfficeAreaCode(String arg) {
			_telephoneNumber.setOfficeAreaCode(arg);
		}
		void setOfficeNumber(String arg) {
			_telephoneNumber.setOfficeNumber(arg);
		}
		
	}
	
	
	class TelephoneNumber {
		private String _officeAreaCode;
		private String _officeNumber;
		
		public TelephoneNumber() {
			_officeAreaCode = "";
			_officeNumber = "";
		}
		
		public String getNumber() {
			return ("(" + _officeAreaCode + ") " + _officeNumber);
		}
		void setOfficeAreaCode(String arg) {
			_officeAreaCode = arg;
		}
		void setOfficeNumber(String arg) {
			_officeNumber = arg;
		}
		String getOfficeAreaCode() {
			return _officeAreaCode;
		}
		String getOfficeNumber() {
			return _officeNumber;
		}
	}
	
}
